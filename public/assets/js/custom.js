/* *************************************** */  
/* Parallax */
/* *************************************** */  

/*$(document).ready(function(){
	$('.parallax-feature').parallax("50%", 0.15);
	$('.parallax-testimonial').parallax("50%", 0.15);
	$('.parallax-service').parallax("50%", 0.2);
	$('.parallax-cta').parallax("50%", 0.2);
	$('.parallax-sponsor').parallax("50%", 0.3);
	$('.parallax-model-two').parallax("50%", 0.3);
	$('.parallax-model-three').parallax("50%", 0.3);
	$('.parallax-video').parallax("50%", 0.3);
	$('.parallax-model-five').parallax("50%", 0.2);
	$('.login-background').parallax("100%", 0.3);
	$('.registration-background').parallax("100%", 0.3);
});*/

/* *************************************** */  
/* Way Points JS */
/* *************************************** */  

var slider;

$(document).ready(function(){

	slider = jQuery('.banner').revolution(
		{
			delay:8000,
			startwidth:1170,
			startheight:550,
			
			hideThumbs:10,
			
			navigationType:"none",	
			
			
			hideArrowsOnMobile:"on",
			
			touchenabled:"on",
			onHoverStop:"on",
			
			navOffsetHorizontal:0,
			navOffsetVertical:20,
			
			stopAtSlide:-1,
			stopAfterLoops:-1,

			shadow:0,
			
			fullWidth:"on",
			fullScreen:"off"
		});

	$('.fantasy-authenticate').click(function(e){
		e.preventDefault();

		var width = 500;
		var height = 500;

		var left = (screen.width/2)-(width/2);
  		var top = (screen.height/2)-(height/2);

		var _url = $(this).attr('href');

		window.open(_url, "Front Row Fantasy Sports - Authorization", "menubar=yes,width=500, height=500, left=" + left + ", top=" + top);
	});

	if(window.opener) {
		var _href = window.location.href;
		_href = _href.split('/');
		_href = _href.pop();

		if(_href == 'frontrow') {
			window.onunload = function(){
				window.opener.location.reload();
			}

			self.close();
		}
	}

	$('.league-row a').click(function(e){
		e.preventDefault();
		$(this).closest('.league-row').nextUntil('.league-row').toggle(400);
	});

	// Hero's Way Points
	$('.hero-logo').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInDown');
	}, { 
		offset: '70%' 
	});
	
	$('.hero-heading h1').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
		
	// Feature Way Points
	$('.feature-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '65%' 
	});
	
	// Author Content Way Points 
	$('.author-content').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '75%' 
	});
	
	// Model Way Points
	$('.model-container h3').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInLeft');
	}, { 
		offset: '60%' 
	});
	
	$('.model-container p').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInRight');
	}, { 
		offset: '60%' 
	});
	
	$('.model-container a').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceInRight');
	}, { 
		offset: '75%' 
	});
	
	// Service Way Points
	$('.service-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Company Way Points
	$('.company-content').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '75%' 
	});
	
	// Blog Way Points
	$('.blog-post').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '60%' 
	});
	
	// About Us Way Points
	$('.about-member').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '65%' 
	});
	
	// Model Two Way Points
	$('.model-two p').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeIn');
	}, { 
		offset: '60%' 
	});
	
	// Model Three Way Points With Count To()
	$('.m-counter').waypoint(function(down){
		if(!$(this).hasClass('stop-counter'))
		{
			$(this).countTo();
			$(this).addClass('stop-counter');
		}
	}, { 
		offset: '80%' 
	});
	
	// Pricing Way Points
	$('.pricing-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Branch Information and Map Location Way Points 
	$('.branch-info').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('flipInY');
	}, { 
		offset: '75%' 
	});
	
	$('.branch-map-area i').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	$('.branch-map-area span').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Video Caption and Video Way Points 
	$('.video-cap-container').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('slideInLeft');
	}, { 
		offset: '75%' 
	});
	
	$('.video-container').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('slideInRight');
	}, { 
		offset: '75%' 
	});
	
	// Model Five Item Way Points 
	$('.m-five-item').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { 
		offset: '75%' 
	});
	
	// Grid Item Way Points
	$('.grid .grid-entry').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInUp');
	}, { 
		offset: '90%' 
	});

	//view terms id
	$('#view_terms').click(function(e){
		e.preventDefault();
		window.open('/assets/terms.html');
	});
});