<?php

namespace Fantasy\Parser;

class YahooParser implements \Fantasy\FantasyInterface {

	public $client;

	public $_leagues;

	public $_games;

	public $_weeks;

	public $_matchups;

	public $_teams;

	protected $_shouldRetry;

	protected $_failureReason;

	public function getClient()
	{
		if(!$this->client) {
			$this->client = \Yahoo::client();
		}

		return $this->client;
	}

	public function authenticated()
	{
		if($token = $this->getClient()->token) {
			if($token->oauth_problem) {
				return false;
			}
		}

		return (bool)$this->getClient()->token;
	}

	public function games()
	{
		if(!$this->_games) {
			$gamesXml = $this->getClient()->getUserGames();
			
			if(!$this->isValid($gamesXml)) {
				if($this->_shouldRetry) {
					return $this->games();
				}
			}

			$usersList = simplexml_load_string($gamesXml);
			$gamesList= $usersList->users->user->games;

			foreach($gamesList->children() as $game) {
				$game_key = (string)$game->game_key;

				$gameWeeksXML = $this->getClient()->getGameWeeks($game_key);

				if(!$this->isValid($gameWeeksXML)) {
					if($this->_shouldRetry) {
						return $this->games();
					}
				}

				$weeksObj = simplexml_load_string($gameWeeksXML);

				$weeks = $weeksObj->game->game_weeks;

				$currenttime = strtotime(date('Y-m-d'));

				foreach($weeks->children() as $week) {
					$start = (string)$week->start;
					$end = (string)$week->end;

					$starttime = strtotime(date($start));
					$endtime = strtotime(date($end));

					if($currenttime >= $starttime && $currenttime <= $endtime) {
						
						$this->_weeks['current'] = array(
							'start' => $start,
							'end' => $end,
							'week' => (string)$week->week
						);
					}

					$this->_weeks['all'][] = array(
							'start' => $start,
							'end' => $end,
							'week' => (string)$week->week
						);
				}

				$this->_games[] = $game_key;
			}
		}

		return $this->_games;
	}

	public function leagues()
	{
		if(!$this->_leagues) {

			$this->_leagues = array();

			foreach($this->games() as $game) {
				$leagueXml = $this->getClient()->getUserLeagues($game);

				if(!$this->isValid($leagueXml)) {
					if($this->_shouldRetry) {
						return $this->leagues();
					}
				}

				$leaguesObj = simplexml_load_string($leagueXml);

				$leagues = $leaguesObj->users->user->games->game->leagues;

				foreach($leagues->children() as $league) {
					$this->_leagues[(string)$league->league_id] = array(
						'gameId' => $game,
						'leagueId' => (string)$league->league_id,
						'leagueKey' => (string)$league->league_key,
						'name' => (string)$league->name,
						'url' => (string)$league->url
					);
				}
			}	
		}

		return $this->_leagues;
	}

	public function teams()
	{
		if(!$this->_teams) {

			foreach($this->leagues() as $game_key => $league) {

				$teamsXml = $this->getClient()->getUserTeams($league['leagueKey']);

				if(!$this->isValid($teamsXml)) {
					if($this->_shouldRetry) {
						return $this->teams();
					}
				}

				$teamsObj = simplexml_load_string($teamsXml);

				$teams = $teamsObj->league->teams;

				foreach($teams->children() as $team) {
					$this->_teams[(string)$league['leagueKey']][] = array(
						'teamKey' => (string)$team->team_key,
						'teamId' => (string)$team->team_id,
						'name' => (string)$team->name,
						'url' => (string)$team->url,
						'logo' => (string)$team->team_logos->team_logo->url,
						'current' => (bool)$team->is_owned_by_current_login
					);
				}
			}
		}

		return $this->_teams;
	}

	public function matchups()
	{
		if(!$this->_matchups) {

			$matchupWeek = $this->_weeks['current']['week'];

			foreach($this->teams() as $league_key => $teams) {
				foreach($teams as $team) {
					if($team['current']) {
						$matchupsXML = $this->getClient()->getMatchups($team['teamKey'], $matchupWeek);

						if(!$this->isValid($matchupsXML)) {
							if($this->_shouldRetry) {
								return $this->matchups();
							}
						}

						$matchupsObj = simplexml_load_string($matchupsXML);

						$matchupTeams = $matchupsObj->team->matchups->matchup->teams;

						$owner = array();
						$opponent = array();

						foreach($matchupTeams->children() as $team) {
							$points = $team->team_points;
							$projected = $team->team_projected_points;

							$player = (string)$team->is_owned_by_current_login == '1' ? 'owner' : 'opponent';

							$this->_matchups[$league_key][$player] = array(
								'url' => (string)$team->url,
								'name' => (string)$team->name,
								'points' => (string)$points->total,
								'projected_points' => (string)$projected->total
							);
						}
					}
				}
			}
		}

		return $this->_matchups;
	}

	public function requestUrl()
	{
		return \Yahoo::request();
	}

	public function isValid( $xml )
	{
		$parser = xml_parser_create();
		xml_parse_into_struct($parser, $xml, $xmlArray);
		
		if($xmlArray[0]['tag'] == 'YAHOO:ERROR') {
			//need to find error message
			preg_match('/"([^"]*)"/', $xmlArray[1]['value'], $errorMsgArray);

			$this->_failureReason = array_pop($errorMsgArray);

			$this->_handleError();

			return false;
		}

		return true;
	}

	protected function _handleError()
	{
		switch($this->_failureReason) {

			case "token_expired":
				$this->getClient()->refreshAccessToken($this->getClient()->token);
				$this->_shouldRetry = true;
				break;
			case "consumer_key_unknown":
				$this->_shouldRetry = true;
				break;
			default:
				print_r($this->_failureReason);
				exit;
		}
	}
}