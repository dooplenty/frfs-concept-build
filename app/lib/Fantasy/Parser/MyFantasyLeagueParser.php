<?php

namespace Fantasy\Parser;

class MyFantasyLeagueParser implements \Fantasy\FantasyInterface {

	public $client;

	public $search;

	protected $_leagues;

  protected $_teams;

  protected $_matchups;

  protected $_userLeagues;

  protected $_provider;

	public $baseUrl = "http://football.myfantasyleague.com/2014/export";

	public function getClient()
	{
		
	}

  public function getProvider()
  {
    if(!$this->_provider)
    {
      $Provider = \Provider::where('providerName', '=', 'MyFantasyLeague')->first();
      $this->_provider = $Provider;
    }

    return $this->_provider;
  }

	public function authenticated()
	{
		$Leagues = $this->_getUserLeagues();

    if(!$Leagues->count()){
      return false;
    }

    return true;
	}

	public function leagues()
	{
		if($this->search) {
			$params = array(
				'TYPE' => 'leagueSearch',
				'SEARCH' => $this->search
			);

			$url = $this->baseUrl;

			$http = $this->fetch($url, $params);

			if($http) {
				$leagueDataXML = $http['response_body'];
				$leagueData = simplexml_load_string($leagueDataXML);

				$leagues = array();
				foreach($leagueData->children() as $league) {
					$leagues[(string)$league->attributes()->id] = array(
						'leagueId' => (string)$league->attributes()->id, 
            'leagueKey' => (string)$league->attributes()->id,
						'name' => (string)$league->attributes()->name,
						'url' => (string)$league->attributes()->homeURL
					);
				}

				$this->_leagues = $leagues;

			}
			
		} else {
      $Leagues = $this->_getUserLeagues();

      $leagues = array();

      foreach($Leagues as $League) {
        $params = array(
          'L' => $League->providerLeagueId,
          'TYPE' => 'league'
        );

        $http = $this->fetch($this->baseUrl, $params);

        if($http) {
          $leagueXml = $http['response_body'];
          $leagueData = simplexml_load_string($leagueXml);

          $leagues[(string)$leagueData->attributes()->id] = array(
            'leagueId' => (string)$leagueData->attributes()->id,
            'leagueKey' => (string)$leagueData->attributes()->id, 
            'name' => (string)$leagueData->attributes()->name,
            'url' => (string)$leagueData->attributes()->homeURL
          );

          $teams = array();

          foreach($leagueData->franchises->children() as $league) {
            $leagueName = (string)$league->attributes()->name;

            $teams[(string)$league->attributes()->id] = array(
              'teamKey' => (string)$league->attributes()->id,
              'teamId' => (string)$league->attributes()->id,
              'name' => (string)$league->attributes()->name,
              'logo' => (string)$league->attributes()->logo,
              'current' => $leagueName == $League->franchiseName ? '1' : '0'
            );
          }

          $this->_teams[(string)$leagueData->attributes()->id] = $teams;
        }
      }

      $this->_leagues = $leagues;
    }

		return $this->_leagues;
	}

	public function games()
	{
		
	}

  public function teams()
  {
    if(!$this->_teams) {
      $this->leagues();
    }

    return $this->_teams;
  }

  /**
   * MyFantasyLeague liveScoring API
   * http://football7.myfantasyleague.com/2012/export?TYPE=liveScoring&L=35465&W=12
   */
  public function matchups()
  {
    if(!$this->_matchups) {
      foreach($this->leagues() as $leagueKey => $league) {
        $params = array(
          'TYPE' => 'liveScoring',
          'L' => $leagueKey
        );

        $http = $this->fetch($this->baseUrl, $params);
        if($http) {
          $matchupXML = $http['response_body'];
          $matchupData = simplexml_load_string($matchupXML);

          //need to know current logged in team id
          $currentTeamId = 0;

          $ownerSet = false;
          foreach($matchupData->matchup as $matchup) {

            if($ownerSet) {
              continue;
            }

            foreach($matchup->franchise as $weekMatchup) {
              $team = $this->teams()[$leagueKey][(string)$weekMatchup->attributes()->id];

              $player = 'opponent';
              if((string)$weekMatchup->attributes()->id == $team['teamId'] && $team['current']) {
                $player = 'owner';
                $ownerSet = true;
              }

              $this->_matchups[$leagueKey][$player] = array(
                'url' => '',
                'name' => $team['name'],
                'points' => (string)$weekMatchup->attributes()->score,
                'projected_points' => (string)$weekMatchup->attributes()->score
              );        
            }
          }
        }
      }
    }

    return $this->_matchups;
  }

	public function requestUrl()
	{
		return "auth/myfantasyleague";
	}

	public function isValid( $xml )
	{

	}

	public function addSearch( $search )
	{
		$this->search = $search;
	}

  protected function _getUserLeagues()
  {
    if($this->_userLeagues) {
      return $this->_userLeagues;
    }

    $User = \User::find(\Auth::user()->userId);
    
    $Provider = $this->getProvider();

    $Leagues = $User->leagues()->where('providerId', '=', $Provider->providerId)->get();

    $this->_userLeagues = $Leagues;

    return $Leagues;
  }

	public static function fetch($url, $params, $headers = array(), $post = null, $options = array())
  {
    $options = array_merge(array(
      'timeout'         => '10',
      'connect_timeout' => '10',
      'compression'     => true,
      'debug'           => true,
      'log'             => sys_get_temp_dir().'/curl_debug.log',
    ), $options);

    if(!empty($params))
    {
      $url = $url.'?'.http_build_query($params);
    }

    $ch = curl_init($url);

    $headers = array_merge($headers, array('Content-Type: application/xml'));

    // return headers
    curl_setopt($ch, CURLOPT_HEADER, true);

    // return body
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // merge headers $request_headers["Content-Type"], "application/x-www-form-urlencoded"

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

    // handle redirects
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    // handle http compression
    curl_setopt($ch, CURLOPT_ENCODING, '');

    // timeouts
    curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $options['timeout']);

    // be nice to dev ssl certs
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FAILONERROR, false);

    // debug curl options
    if($options['debug'])
    {
      curl_setopt($ch, CURLINFO_HEADER_OUT, true);
      curl_setopt($ch, CURLOPT_VERBOSE, true);
    }

    $http = array();
    $response = curl_exec($ch);

    if($response === false)
    {
      // something bad happeneed (timeout, dns failure, ???)
      error_log(curl_error($ch));
      return false;
    }

    // process response
    $http = array_merge($http, curl_getinfo($ch));
    
    // process status code + headers
    list($junk, $http['response_header'], $http['response_body']) = explode("\r\n\r\n", $response, 3);
    $response_header_lines = explode("\r\n", $http['response_header']);
    $http_response_line = array_shift($response_header_lines);
    if (preg_match('@^HTTP/[0-9]\.[0-9] ([0-9]{3})@', $http_response_line, $matches)) {
      $http['response_code'] = $matches[1];
    }
    $http['response_headers'] = array();
    foreach ($response_header_lines as $header_line) {
        list($header, $value) = explode(': ', $header_line, 2);
        $http['response_headers'][$header] = $value;
    }

    curl_close($ch);

    return $http;
  }
}