<?php

namespace Fantasy;

interface FantasyInterface {

	public function getClient();

	public function authenticated();

	public function leagues();

	public function games();

	public function requestUrl();

	public function isValid( $xml );


}