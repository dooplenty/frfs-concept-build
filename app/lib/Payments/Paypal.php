<?php
namespace Payments;

use PayPal\Api\PaymentExecution;

use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class Paypal {

	protected $payment;

	protected $transaction;

	public function __construct( $redirectBase )
	{
		$payment = new Payment();
		
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($redirectBase . "/success");
		$redirectUrls->setCancelUrl($redirectBase . "/cancel");

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$payment->setPayer($payer);
		$payment->setRedirectUrls($redirectUrls);
		$payment->setIntent("sale");

		$this->payment = $payment;
	}

	public function setupTransaction($total, $description)
	{
		$amount = new Amount();
		$amount->setCurrency('USD');
		$amount->setTotal('19.99');

		$transaction = new Transaction();
		$transaction->setAmount($total);
		$transaction->setDescription($description);

		$this->transaction = $transaction;
	}

	public function makePayment()
	{
		$this->payment->setTransactions(array($this->transaction));
		$this->payment->create($this->getApiContext());
	}

	protected function getApiContext()
	{
		return $apiContext = new ApiContext(new OAuthTokenCredential(
			'AZc2QhCGyhJH_z4i1j7Q_xOo3ugfZDxGdNxq2V7nEEBeugbwiUwg9TToutJg',
			'EI1TzxCPhPGJeVZKowDtBEptvGHp9iZCEL1nlJjyt9WZwrVOW0N0xRNU8fcb'
		));
	}
}