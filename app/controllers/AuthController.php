<?php

class AuthController extends \BaseController {

	public $layout = 'layouts.master';

	/**
	 * Authenticate service
	 *
	 * @return Response
	 */
	public function customAuth($driver)
	{
		switch($driver)
		{
			case 'myfantasyleague':
				$this->layout->content = View::make('frontrow.myfantasyleague.access');
		}
	}

	public function search($driver)
	{
		$leagues = array();

		switch($driver)
		{
			case 'myfantasyleague':
				$Parser = new Fantasy\Parser\MyFantasyLeagueParser;
				$Parser->addSearch(Input::get('searchCriteria'));
				
				$leagues = $Parser->leagues();
				break;
		}

		$this->layout->content = View::make('frontrow.myfantasyleague.leagues', array('leagues' => $leagues, 'driver' => $driver));
	}

	public function save($driver)
	{
		$leagues = Input::get('league');
		$franchises = Input::get('franchise');
		
		//get the provider for this league
		$Provider = Provider::where('providerName', '=', $driver)->first();
		
		//make sure we have a user/provider record
		$User = User::find(Auth::user()->userId);
		$UserProviders = $User->providers();
		$providerResult = $UserProviders->wherePivot('providerId', '=', $Provider->providerId);

		if(!$providerResult->get()->first()){
			$User->providers()->attach($Provider->providerId);
		}

		foreach($leagues as $leagueId) {
			$League = new League(array(
				'providerId' => $Provider->providerId,
				'providerLeagueId' => $leagueId,
				'userId' => $User->userId,
				'franchiseName' => $franchises[$leagueId]
			));

			$League->save();
		}

		return Redirect::to('frontrow');
	}

}
