<?php

class UserController extends BaseController {
	
	public $layout = 'layouts.master';

	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => 'post'));
	}

	public function showLogin()
	{
		$this->layout->content = View::make('login');
	}

	public function doLogin()
	{
		$credentials = array(
			'username' => Input::get('username'), 
			'password' => Input::get('password'),
			'confirmed' => 1
		);

		$remember = Input::get('remember_me') ? true : false;

		if( Auth::attempt( $credentials, $remember ) ) {
			return Redirect::intended('frontrow');
		} else {
			return Redirect::to('user/login')
				->with('message', 'Your username/password combination was incorrect')
				->withInput();
		}
	}

	public function showSignup()
	{
		$this->layout->content = View::make('sections.sign-up');
	}

	public function doSignup()
	{
		$User = new User(Input::except('fantasyteam'));

		$validation = Validator::make(
			Input::all(),
			array(
				'email' => 'required|email|unique:users',
				'password' => 'required|alphaNum|min:8|confirmed',
				'username' => 'required|unique:users',
				'firstName' => 'required',
				'lastName' => 'required',
				'termsAccepted' => 'accepted'
			)
		);

		if( !$validation->fails() ) {
			$confirmation = str_shuffle(sha1($User->email.spl_object_hash($this).microtime(true)));
			$confirmation = hash_hmac('sha1', $confirmation, Config::get('app.key'));

			$User->setAttribute('confirmation', $confirmation);
			
			if($User->save()) {
				Mail::send('emails.confirmation', array('user' => $User), function($message) use ($User) {
					$message->from('no-reply@frontrowfs.com', 'Front Row Fantasy Sports - No Reply');
					$message->to($User->email, $User->firstName . ' ' . $User->lastName);
					$message->subject('Welcome to Front Row Fantasy Sports!');
				});

				return Redirect::to('user/login')->with('message', 'Welcome to Front Row! Login to get started!');
			} else {
				return Redirect::to('user/signup')->with('message', 'There was an issue saving your registration information. Please try again. If you continue to have trouble contact us at support@frontrowfs.com');
			}

		} else {
			return Redirect::to('user/signup')->withErrors($validation);
		}
	}

	public function doLogout()
	{
		Auth::logout();

		return Redirect::to('/user/login')->with('message', 'You have been logged out successfully.');
	}

	public function confirmUser($user_token)
	{
		$User = User::where('confirmation', $user_token);

		if($User) {
			$User->update(array('confirmed' => 1));

			return Redirect::to('/user/login')->with('message', 'You have been confirmed. You may now login using your username/password combination.');
		}
	}
}