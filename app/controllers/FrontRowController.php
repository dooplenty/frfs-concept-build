<?php

class FrontRowController extends BaseController {

	public $layout = 'layouts.master';

	public $providers = array('Yahoo', 'MyFantasyLeague');

	public function __construct()
	{
		$this->beforeFilter('auth');
	}

	public function getIndex()
	{
		$providers_array = array();

		//TODO for now providers will come from specified array here
		//eventually users will subscribe
		foreach($this->providers as $provider) {
			$ParserClass = "Fantasy\Parser\\{$provider}Parser";
			$Parser = new $ParserClass;

			$providers_array[$provider] = array(
				'authenticated' => $Parser->authenticated(),
				'url' => $Parser->requestUrl()
			);

			if($Parser->authenticated()) {
				$providers_array[$provider]["leagues"] = $Parser->leagues();
				$providers_array[$provider]["teams"] = $Parser->teams();
				$providers_array[$provider]["matchups"] = $Parser->matchups();
			}
		}

		$this->layout->content = View::make('frontrow', array('providers' => $providers_array));
	}

}
