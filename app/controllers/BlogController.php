<?php

class BlogController extends \BaseController {

	public $layout = "layouts.master";

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$posts = Post::orderBy('postId','desc')->paginate(10);
        $this->layout->content = View::make('posts', compact('posts'));
	}


	/**
	 * Search for blog posts
	 *
	 * @return Response
	 */
	public function getSearch()
	{
		$searchTerm = Input::get('search');
		$posts = Post::whereRaw("title LIKE ? OR content LIKE ?", ["%$searchTerm%", "%$searchTerm%"])->paginate(10);
		$this->layout->content = View::make('posts', compact('posts'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
