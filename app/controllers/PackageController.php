<?php

class PackageController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('paypal');
	}

	public function getIndex()
	{
		$this->layout->content = View::make('packages.main');
	}

	public function purchase()
	{
		$packageId = Request::get('fantasyPackage');
		$Package = Package::find($packageId)->first();

		$redirectBase = str_replace(Request::segment(2), 'confirmation', Request::url());

		$payment = new Payments\Paypal($redirectBase);
		$payment->setupTransaction($Package->packagePrice, $Package->packageName);
		$payment->makePayment();

	}

	public function confirmation( $method )
	{
		$this->layout->content = View::make('packages.confirmation', compact('method'));
	}

}