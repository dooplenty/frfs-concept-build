<?php
class PasswordController extends BaseController {

	public $layout = 'layouts.master';

	public function remind()
	{
		$this->layout->content = View::make('password.remind');
	}

	public function request()
	{
		$credentials = array('email' => Input::get('email'), 'password' => Input::get('password'));

		$response = Password::remind($credentials);

		switch($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_USER:
			case Password::INVALID_TOKEN:
				return Redirect::back()->with('error', Lang::get($response));

			case Password::PASSWORD_RESET:
			case Password::REMINDER_SENT:
				return Redirect::to('user/login')->with('message', 'Password reset email sent. Be sure to check your junk folder if you are unable to find it.');
		}
	}

	public function reset($token)
	{
		$this->layout->content = View::make('password.reset')->with('token', $token);
	}

	public function update()
	{
		$credentials = Input::only('email', 'password', 'password_confirmation', 'token');

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = $password;
			$user->save();
		});

		switch($response)
		{
			case Password::PASSWORD_RESET:
				return Redirect::to('user/login')->with('flash', 'You may now login with your new password.');
				break;
			default:
				return Redirect::back()->with('error', Lang::get($response));
				break;
		}
	}
}