<?php

class PostController extends \BaseController {

	public $layout = "layouts.master";

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function showPost(Post $post)
	{
		
		$this->layout->content = View::make('posts.post', array('post' => $post));

	}
}
