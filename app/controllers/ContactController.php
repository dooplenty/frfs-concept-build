<?php

class ContactController extends BaseController {

	public $layout = 'layouts.master';

	public function getIndex()
	{
		$this->layout->content = View::make('contact');
	}

	public function signUpPost()
	{
		$name = Input::get('name');
		$email = Input::get('email');
		$fantasyteams = Input::get('fantasyteam');

		if($fantasyteams){
			$fantasyteams = implode(",", array_keys($fantasyteams));
		}

		DB::insert('insert into signups (name, email, provider) values(?,?,?)', array($name, $email, $fantasyteams));

		Return Redirect::to('/');
	}

}
