<?php

class IpnController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$order = IPN::getOrder();
		if($order->payment_status == 'Completed') {
			$username = $order->custom;
			$user = User::where('username', '=', $username)->first();
			$userPackage = new UserPackage;
			$userPackage->userId = $user->userId;
			$userPackage->packageId = 1;
			$userPackage->save();
		}

		return "success";
	}


}
