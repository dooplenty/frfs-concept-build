<?php

class HomeController extends BaseController {

	public $layout = 'layouts.master';

	public function getIndex()
	{
		$this->layout->content = View::make('index');
	}

	public function getResources()
	{
		$this->layout->content = View::make('resources');
	}

	public function show2015()
	{
		$this->layout->content = View::make('coming-soon');
	}

}
