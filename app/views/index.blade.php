@include('sections.video');

<div class="inner-page">
	<div class="hero">
		<div class="container">
			<div class="hero-heading">
				<div class="hero-logo animated">
					<img src="assets/imgs/logo-hero.png" />
				</div> <!-- /.hero-logo -->

				<h1 class="animated">Manage All Fantasy Teams from <strong>ONE</strong> Place<br /> <strong>Front Row Fantasy Sports</strong></h1>
			</div> <!-- /.hero-heading -->
		</div> <!-- /.container -->
	</div> <!-- /.hero -->
</div> <!-- /.inner-page -->

@include('sections.sign-up')