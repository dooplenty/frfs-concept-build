<!-- Side Bar Start -->
<div class="col-md-4 col-sm-4">
	<div class="inner-sidebar">
		<!-- Sidebar widget -->
		<!-- First -->
		<div class="side-widget">
			<h4>Search</h4>
			<div class="widget-content">
				<!-- search button and input box -->
				{{ Form::open(array('url' => '/blog/search', 'class' => 'form-inline bform', 'role' => 'form', 'method' => 'post')) }}
					<div class="form-group">
						<input name="search" type="text" class="form-control" placeholder="Type to search">
					</div>
					<button type="submit" class="btn btn-info">Search</button>
				{{ Form::close() }}
			</div>
		</div>
		<!-- Second -->
		<div class="side-widget">
			<h4>Recent Posts</h4>
			<div class="widget-content">
				<ol>
					@foreach($recentPosts as $recent)
						<li><a href="#">{{ link_to_route('post.show', $recent->title, $recent->postId) }}</a></li>
					@endforeach
				</ol>
			</div>
		</div>
		<!-- Third -->
		<div class="side-widget">
			<!-- Feedzilla Widget BEGIN -->

			<div class="feedzilla-news-widget feedzilla-9327299285214394" style="width:250px; padding: 0; text-align: center; font-size: 11px; border: 0;">
			<script type="text/javascript" src="http://widgets.feedzilla.com/news/iframe/js/widget.js"></script>
			<script type="text/javascript">
			new FEEDZILLA.Widget({
				style: 'slide-top-to-bottom',
				culture_code: 'en_us',
				q: 'fantasy football',
				headerBackgroundColor: '#b70000',
				footerBackgroundColor: '#b70000',
				title: 'Fantasy News',
				order: 'relevance',
				count: '10',
				w: '250',
				h: '300',
				timestamp: 'true',
				scrollbar: 'true',
				theme: 'ui-lightness',
				className: 'feedzilla-9327299285214394'
			});
			</script><br />
			</div>

			<!-- Feedzilla Widget END -->
		</div>
		<!-- Fourth -->
		<!-- <div class="side-widget">
			<h4>About Us</h4>
			<div class="widget-content">
				<p>Foresee the pain ancondb imentum rutrum aliquet. Quisque eu consectetur erat. Proin rutrum, erat egd trouble that are bound</p>
				<div class="social">
					<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
					<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
					<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
					<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
				</div>
			</div>
		</div> -->
	</div>
</div>
<!-- Side BAr End -->