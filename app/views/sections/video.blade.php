<div class='banner-container'>
	<div class='banner'>
		<ul>
			<li data-transition="fade" data-slotamount="5" data-masterspeed="700">
				<!-- MAIN IMAGE -->
				<img src="assets/imgs/slider/transparent.png"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
				
				<!-- LAYERS NR. 1 // i-phone -->
				<div class="tp-caption lfl"
					data-x="80"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="1200"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s13.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 2 // i-phone -->
				<div class="tp-caption lfl"
					data-x="190"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="1800"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s12.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 3 // i-phone -->
				<div class="tp-caption lfl"
					data-x="320"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="2400"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s11.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 4 // Slide Heading -->
				<div class="tp-caption lfl largeblackbg slide-one heading"
					data-x="620"
					data-y="160"
					data-speed="1500"
					data-start="3000"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">Front Row Fantasy Sports
				</div>
				<!-- LAYERS NR. 5 // Slide Paragraph -->
				<div class="tp-caption sfb medium_text slide-one paragraph"
					data-x="620"
					data-y="240"
					data-speed="1500"
					data-start="3300"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">Fantasy Sports management in one location<br /> across multiple sports providers.
				</div>
				<!-- LAYERS NR. 6 // Slide Button -->
				<div class="tp-caption sfb"
					data-x="620"
					data-y="325"
					data-speed="1500"
					data-start="3600"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><a href="<?= action('UserController@doSignup') ?>" class="btn btn-info btn-lg">Join Us</a>&nbsp;
											<a href="<?= action('PackageController@getIndex') ?>" class="btn btn-danger btn-lg">Buy Now</a>
				</div>
			</li>

			<!-- SLIDE NR. 2 -->
			<li data-transition="slidedown" data-slotamount="5" data-masterspeed="700" >
				<!-- MAIN IMAGE -->
				<img src="assets/imgs/slider/transparent.png"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
				
				<!-- LAYERS NR. 1 // i-pad -->
				<div class="tp-caption lfr"
					data-x="right"
					data-hoffset="-280"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="1800"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s21.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 2 // i-pad -->
				<div class="tp-caption lfr"
					data-x="right"
					data-hoffset="-80"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="2400"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s22.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 3 // Heading -->
				<div class="tp-caption lfl largeblackbg slide-two heading"
					data-x="left"
					data-hoffset="80"
					data-y="110"
					data-speed="1500"
					data-start="2400"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">Simplified Layout
				</div>
				<!-- LAYERS NR. 4.1 // Text with Icon -->
				<div class="tp-caption sfb medium_bg_red slide-two slide-text"
					data-x="left"
					data-hoffset="80"
					data-y="200"
					data-speed="1500"
					data-start="3000"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><i class="fa fa-check"></i> View Scores
				</div>
				<!-- LAYERS NR. 4.2 // Text with Icon -->
				<div class="tp-caption sfb medium_bg_red slide-two slide-text"
					data-x="left"
					data-hoffset="80"
					data-y="250"
					data-speed="1500"
					data-start="3300"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><i class="fa fa-check"></i> View Roster
				</div>
				<!-- LAYERS NR. 4.3 // Text with Icon -->
				<div class="tp-caption sfb medium_bg_red slide-two slide-text"
					data-x="left"
					data-hoffset="80"
					data-y="300"
					data-speed="1500"
					data-start="3600"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><i class="fa fa-check"></i> Update Roster
				</div>
				<!-- LAYERS NR. 5 // Slide Button -->
				<div class="tp-caption sfb"
					data-x="left"
					data-hoffset="80"
					data-y="360"
					data-speed="1500"
					data-start="3600"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><a href="<?= action('PackageController@getIndex') ?>" class="btn btn-danger btn-lg">Get It Now</a>&nbsp;
				</div>
			</li>

			<!-- SLIDE NR. 3 -->
			<li data-transition="slidedown" data-slotamount="5" data-masterspeed="700" >
				<!-- MAIN IMAGE -->
				<img src="assets/imgs/slider/transparent.png"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
				
				<!-- LAYERS NR. 1 // Mac Desktop -->
				<div class="tp-caption lfl"
					data-x="center"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="2800"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s31.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 2 // Mac Book -->
				<div class="tp-caption lfr"
					data-x="560"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="3400"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s32.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 3 // i-Pad -->
				<div class="tp-caption lfl"
					data-x="300"
					data-y="bottom"
					data-voffset="-50"
					data-speed="1500"
					data-start="4000"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off"><img src="assets/imgs/slider/s33.png" class="img-responsive" alt="Slide Image" />
				</div>
				<!-- LAYERS NR. 4 // Heading -->
				<div class="tp-caption lfl largeblackbg slide-three heading"
					data-x="center"
					data-y="40"
					data-speed="1500"
					data-start="1800"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">Quick Access on any Platform
				</div>
				<!-- LAYERS NR. 5 // Slide Paragraph -->
				<div class="tp-caption lfr medium_text slide-three paragraph"
					data-x="center"
					data-y="120"
					data-speed="1500"
					data-start="2100"
					data-easing="Power4.easeOut"
					data-endspeed="300"
					data-endeasing="Linear.easeNone"
					data-captionhidden="off">Whether you're viewing on desktop, tablet, or phone<br />You will be able to access fantasy content quickly.
				</div>
			</li>
		</ul>
	</div>
</div>