<div class="contact padd">
	<div class="container">
		<div class="heading">
			<!-- Heading -->
			<h2>Join Us Now!</h2>
			<!-- Sub Heading -->
			<h6>Experience Fantasy Sports from a Different Seat</h6>
			<hr />
		</div>

		<!-- Contact Item -->
		<div class="contact-item">
			<!-- Paragraph -->
			<p><i class="fa fa-sign-in"></i> Beta Promo! Join us now to get an additional year FREE!</p>
		</div>
		<!-- Contact Container -->
		<div class="contact-container">
			<!-- Border -->
			<div class="contact-border"></div>

			@foreach( $errors->all() as $error )
				<div class='alert alert-danger'>{{ $error }}</div>
			@endforeach

			<div class="row">
				<!-- Form -->
				{{ Form::open(array('url' => 'user/signup', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post')) }}
					<div class="col-md-5 col-sm-4">
						<div class="form-group">
							{{ Form::label('firstName', 'First Name', array('class' => 'col-lg-3 control-label')) }}
							<div class="col-lg-9">
								<input type="text" class="form-control" id="firstName" placeholder="First Name" name="firstName" value="{{ Input::old('firstName') }}">
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('lastName', 'Last Name', array('class' => 'col-lg-3 control-label')) }}
							<div class="col-lg-9">
								<input type="text" class="form-control" id="lastName" placeholder="Last Name" name="lastName" value="{{ Input::old('lastName') }}">
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('email', 'Email', array('class' => 'col-lg-3 control-label')) }}
							<div class="col-lg-9">
								<input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{ Input::old('email') }}">
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('username', 'Username', array('class' => 'col-lg-3 control-label')) }}
							<div class="col-lg-9">
								<input type="text" class="form-control" id="username" placeholder="Username" name="username" value="{{ Input::old('username') }}">
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('password', 'Password', array('class' => 'col-lg-3 control-label')) }}
							<div class="col-lg-9">
								<input type="password" class="form-control" id="password" placeholder="Password" name="password">
							</div>
						</div>
						<div class="form-group">
							{{ Form::label('password_confirmation', 'Confirm Password', array('class' => 'col-lg-3 control-label')) }}
							<div class="col-lg-9">
								<input type="password" class="form-control" id="password_confirmation" placeholder="Confirm Password" name="password_confirmation">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3"></div>
							<div class="col-sm-9">
								<div class="checkbox">
									<label>
										{{ Form::checkbox('termsAccepted', true) }}
										Accept All Terms &amp; Conditions
									</label>
									<span class="pull-right"><a href="#" id="view_terms" class="btn btn-default btn-xs" target="_blank">View</a></span>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-9">
								<button type="submit" class="btn btn-info">Join</button> &nbsp;
							</div>
						</div>
					</div>
					<div class="col-md-7 col-sm-8">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<!-- Contact widget -->
								<div class="contact-widget">
									<!-- Icon -->
									<i class="fa fa-users c-icon"></i>
									<div class="cw-details">
										<!-- Headiing -->
										<h4>Current Fantasy Teams</h4>
										
										<div class="form-group">
											<div class="col-lg-9">
												<div class='row'>
													<div class='col-lg-3'>
														<p><input type='checkbox' name='fantasyteam[yahoo]' /> Yahoo</p>
														<p><input type='checkbox' name='fantasyteam[espn]' /> ESPN</p>
														<p><input type='checkbox' name='fantasyteam[nfl]' /> NFL</p>
														<p><input type='checkbox' name='fantasyteam[cbs]' /> CBS</p>
													</div>
													<div class='col-lg-6'>
														<p><input type='checkbox' name='fantasyteam[nffc]' /> NFFC</p>
														<p><input type='checkbox' name='fantasyteam[myfantasyleague]' /> My Fantasy League</p>
														<p><input type='checkbox' name='fantasyteam[rtfs]' /> RealTime Fantasy Sports</p>
														<p><input type='checkbox' name='fantasyteam[other]' /> Other</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>