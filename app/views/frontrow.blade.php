<div class="feature padd parallax-feature">
	<div class="container">
		<div class="row">
			@foreach ($providers as $provider => $creds)
				<div class="col-md-3 col-sm-6">
					<!-- Feature Item -->
					<div class="feature-item animated">
						<a href="http://football.fantasysports.yahoo.com/" target="_blank" class='fantasy-hero'>
							<!-- Flat Icon Image -->
							{{ HTML::image('assets/imgs/' . $provider . '-logo.png', 'Yahoo Fantasy Sports', array('class' => 'img-responsive img-thumbnail')) }}
							<!-- Title -->
							<span class="f-item-title">{{ strtoupper($provider) }}</span>
						</a>
						
						@if($creds['authenticated'])
							@include('frontrow.' . strtolower($provider) . '.authorized')
						@else
							@include('frontrow.authenticate')
						@endif
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>