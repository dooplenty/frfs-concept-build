<div class="inner-page padd">
	<div class="inner-blog">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<!-- The new post done by user's all in the post block -->
					<div class="blog-post">
						<div class="entry">
							<h3>{{link_to_route('post.show', $post->title, $post->postId) }}</h3>

							<!-- Meta for this block -->
							<div class="meta">
								<i class="fa fa-calendar"></i>&nbsp; {{explode(' ',$post->created_at)[0]}} &nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp; 
								{{ link_to("http://www.sportetics.com/", "PJ Tebin", array('target' => '_blank')) }}
							</div>

							<div class="blog-img">
								{{ HTML::image('assets/imgs/blog/'.$post->imgFilename, $post->title, array('class' => 'img-responsive img-thumbnail')) }}
								<div class='photo-credit'>{{ $post->metaContent }}</div>
							</div>

							<div class='entry-content'>
								{{ $post->content }}
							</div>
						</div>
					</div>
					<!-- Blog post End -->
				</div>

				@include('sections.sidebar')
			</div>
		</div>
	</div>
</div>