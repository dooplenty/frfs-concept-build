<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p><img src='{{ asset('assets/imgs/logo-symbol.png') }}' /></p>
		<h3>Welcome {{ ucfirst($user->firstName) }} {{ ucfirst($user->lastName) }},</h3>

		<p>You are almost done completely your sign up and simplifying your fantasy life! Just follow the link below to confirm your email address.</p>

		<p><a href="http://frontrowfantasysports.com/user/confirm/{{ $user->confirmation }}">http://frontrowfantasysports.com/user/confirm/{{ $user->confirmation }}</a></p>
	</body>
</html>


