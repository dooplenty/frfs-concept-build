<div class='social-login'>
	<div class='row'>
		<div class='col-md-12 col-sm-12'>
			<a href="{{ $creds['url'] }}" class="btn btn-lg facebook fantasy-authenticate">
				Add {{ $provider }} <i class="fa fa-plus-circle fa-2x pull-right"></i>
			</a>
		</div>
	</div>
</div>