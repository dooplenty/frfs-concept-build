@foreach( $providers['MyFantasyLeague']['leagues'] as $league )
	<div class='row br-blue league-row'>
		<div class='col-md-12 col-sm-12'>
			<a href='#'><span class='feature-league-title'> {{ $league['name'] }}<i class='fa fa-arrow-down'></i></span></a>
		</div>
	</div>
@endforeach

@foreach( $providers['MyFantasyLeague']['matchups'][$league['leagueKey']] as $player => $matchup )
	@if( $player == 'owner' )
		<div class='row br-black vs-row'>
	@else
		<div class='row br-grey vs-row'>
	@endif
		<a class='fantasy-team' href='#'>
			<div class='col-md-8 col-sm-8 col-xs-8 col-large-8 text-left'>
				<strong>{{ $matchup['name'] }}</strong>
			</div>
			<div class='col-md-4 col-sm-4 col-xs-4 text-right label'>
				{{ $matchup['points'] }}
			</div>
		</a>
	</div>
@endforeach