<div class='inner-page padd'>
	<div class='container'>
		<h2 class='text-center'>Enter Search Criteria</h2>
		{{ Form::open(array('url' => 'auth/search/myfantasyleague', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post')) }}
			<div class='form-group'>
				<div class='col-sm-12'>
					<input type='text' name='searchCriteria' id='searchCriteria' class='form-control' placeholder='Email or League Name' />
				</div>
			</div>

			<div class='form-group'>
				<div class='col-sm-12'>
					<input type='submit' name='search' id='search' value='Search' class='form-control' />
				</div>
			</div>
		{{ Form::close() }}
	</div>
</div>