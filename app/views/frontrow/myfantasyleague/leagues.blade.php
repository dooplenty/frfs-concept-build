<div class='inner-page padd'>
	<div class='container'>
		<h2 class='text-center'>Choose Leagues</h2>
		{{ Form::open(array('url' => 'auth/leagues/save/myfantasyleague', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post')) }}
			<div class='form-group'>
				<div class='col-sm-12'>
					@foreach($leagues as $league)
						<div class='row form-group'>
							<div class='col-xs-3 text-right'><input type='checkbox' name='league[{{ $driver }}]' value='{{ $league['leagueId'] }}' /></div>
							<div class='col-xs-9'> {{ $league['name'] }}</div>
						</div>
						<div class='row form-group'>
							<div class='col-xs-3 text-right'><label>Franchise</label></div>
							<div class='col-xs-9'><input type='text' name='franchise[{{ $league['leagueId'] }}]' class='form-control' /></div>
						</div>
						<hr />
					@endforeach
					<div class='row'>
						<div class='col-xs-12 text-center'>
							<input type='submit' value='Save' class='btn btn-primary'/>
						</div>
					</div>
				</div>
			</div>
		{{ Form::close() }}
	</div>
</div>