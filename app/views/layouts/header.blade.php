@section('header')
<div class='header'>
	<!-- Navigation Bar -->
	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#frfs-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="/"><span class="logo"><i></i><!-- <i class="fa fa-crosshairs"></i> --></span></a>
			</div>

			<div class="collapse navbar-collapse" id="frfs-navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?= action('HomeController@getIndex') ?>">Home</a></li>
					<li><a href="<?= action('HomeController@show2015') ?>">2015 Vision!</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="<?= action('FrontRowController@getIndex') ?>">My Front Row <span class="fa fa-angle-down"></span></a>
						<ul class="dropdown-menu" role="menu">
							@if( Auth::guest() )

							<li><a href="<?= action('UserController@showSignup') ?>">Join Us</a></li>
							<li><a href="<?= action('UserController@showLogin') ?>">Login</a></li>

							@else
							
							<li><a href="<?= action('FrontRowController@getIndex') ?>">My FrontRow</a></li>
							<li><a href="<?= action('UserController@doLogout') ?>">Logout</a></li>
							@endif
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div> <!-- /.container -->
	</nav>
</div>
@show
