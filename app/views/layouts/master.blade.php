<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Front Row Fantasy Sports</title>

		<meta name="description" content="@yield('meta-description', 'Front Row Fantasy Sports is a fantasy sports management tool that allows you to integrate multiple fantasy providers, such as Yahoo, ESPN, CBS, NFL, and MyFantasyLeague into one place.')">
		<meta name="keywords" content="@yield('meta-keywords', 'MyFantasyLeague, NFL, Yahoo, ESPN, CBS, fantasy football,fantasy basketball, fantasy baseball, fantasy sports')">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">

		<!-- Styles -->
		@section('styles')
			{{ HTML::style('assets/css/bootstrap/bootstrap.min.css') }}
			{{ HTML::style('assets/css/font-awesome/font-awesome.min.css') }}
			{{ HTML::style('assets/css/animate/animate.min.css') }}
			{{ HTML::style('assets/css/style.css') }}
			{{ HTML::style('assets/css/colors.css') }}
		@show
	</head>

	<body>
		<!-- Wrapper -->
		<div class="wrapper">
			@include('layouts.header')
			
			{{ $content }}

			@include('layouts.footer')
		</div>
	</body>
</html>