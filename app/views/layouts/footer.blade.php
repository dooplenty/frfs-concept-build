<div class='footer'>
	<div class='container'>

		<!-- Social Media -->
		<div class="social">
			<a href="http://facebook.com/myfrontrowfs" class="facebook"><i class="fa fa-facebook"></i></a>
			<a href="http://twitter.com/myfrontrow" class="twitter"><i class="fa fa-twitter"></i></a>
			<!-- <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a> -->
			<a href="https://www.linkedin.com/company/5226959?trk=prof-0-ovw-curr_pos" class="linkedin"><i class="fa fa-linkedin"></i></a>
		</div>

		<!-- Copy right -->
		<div class='copy-right'>
			<p>&copy; Copyright 2014 <a href="/">Front Row Fantasy Sports, LLC</a></p>
		</div>

		<div class='copy-right'>
			<p><a href="mailto:admin@frontrowfs.com?Subject=FRFS - Support">Need Support? Contact Us</a></p>
		</div>

		{{ HTML::image('assets/imgs/2014-fsta-member.png', 'FSTA Member 2014', array('class' => 'fsta-member')) }}
	</div>
</div>

@section('scripts')
	{{ HTML::script('assets/js/jquery/jquery.js') }}
	{{ HTML::script('assets/js/jquery/jquery.prettyPhoto.js') }}
	{{ HTML::script('assets/js/jquery/themepunch/jquery.themepunch.revolution.min.js') }}
	{{ HTML::script('assets/js/jquery/themepunch/jquery.themepunch.plugins.min.js') }}
	{{ HTML::script('assets/js/bootstrap/bootstrap.min.js') }}
	{{ HTML::script('assets/js/waypoints/waypoints.min.js') }}
	{{ HTML::script('assets/js/custom.js') }}
@show

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51896909-1', 'auto');
  ga('send', 'pageview');

</script>