<div class="inner-page">
	<!-- Inner Page Login Page With Back Ground -->
	<div class="inner-login login-background">
		<div class="container">
			@if (Session::has('message'))
				<div class="flash alert">
					<p>{{ Session::get('message') }}</p>
				</div>
			@endif
				
			<div class="login-container">
				<!-- Heading -->
				<h2>Login</h2>
				<!-- Sign in Form Start -->

				@if ($errors->any())
					<ul>
						{{ implode('', $errors->all('<li class="error">:message</li>')) }}
					</ul>
				@endif
	
				{{ Form::open(array('url' => 'user/login', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post')) }}
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control" name="username" id="username" placeholder="Username">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="password" class="form-control" name="password" id="password" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<p class="text-primary"><input type="checkbox" name="remember_me" /> <label><strong>Remember Me</strong></label></p>
							<p class="text-primary"><a class="white" href="<?= action('PasswordController@remind') ?>">Forgot Password?</a></p>
						</div>
					</div>
					<div class="form-group text-center">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-danger">Login</button>&nbsp;
						</div>
					</div>
				{{ Form::close() }}
				<!-- Sign in form End -->
			</div>
		</div>
	</div>
</div>