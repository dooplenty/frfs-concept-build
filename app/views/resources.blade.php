<div class="feature padd">
	<div class="container">
		<div class="col-md-3">
			<div class="feature-item animated fadeInUp">
				<a href="<?= asset('assets/documents/2014cheatsheet.pdf') ?>">
					<img src="<?= asset('assets/imgs/icons/document.png') ?>" />

					<span class="f-item-title">Cheat Sheet</span>
				</a>

				<p>2014 Fantasy Draft Cheat Sheet</p>
			</div>
		</div>
	</div>
</div>