<!-- Video Start -->			
<div class="video padd parallax-video">
	<div class="container">
		<div class="heading shadow">
			<!-- Sub Heading -->
			<h6>How can we simplify your "Fantasy" life...</h6>
			<hr />
		</div>
		<!-- Video Content -->
		<div class="video-content">
			<div class="row">
				<div class="col-md-12">
					<img class='img-responsive' src="{{URL::to('assets/imgs/fantasy-2015.png') }}" />
				</div>
			</div>
		</div>

		<!-- <div class="heading shadow">
			<h2><a href="<?= action('HomeController@show2015') ?>">Check out Front Row 2015</a></h2>
			<hr />
		</div> -->
	</div>
</div>
<!-- Video End -->