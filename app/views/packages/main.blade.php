<div class="feature padd parallax-feature">
	<div class="container">
		<div class="row">
			<input type='hidden' value='1' name='fantasyPackage'>
			<div class="col-md-3 col-sm-12">
				<div class="feature-item animated">
					<a href="#" class='fantasy-hero'>
						<!-- Flat Icon Image -->
						{{ HTML::image('assets/imgs/logo-symbol.png', 'Front Row Fantasy Sports', array('class' => 'img-responsive img-thumbnail')) }}
						<!-- Title -->
						<span class="f-item-title">Beta Promo - 10K*</span>
					</a>

					<div class='social-login'>
						<div class='row'>
							<div class='col-md-12 col-sm-12'>
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
									<input type="hidden" name="cmd" value="_s-xclick">
									<input type="hidden" name="custom" value="{{ Auth::user()->username }}">
									<input type="hidden" name="hosted_button_id" value="4JSP3FEJZWYR8">
									<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
									<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
								</form>


								<!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
									<input type="hidden" name="cmd" value="_s-xclick">
									<input type="hidden" name="hosted_button_id" value="XW2BG6UD34CPL">
									<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
									<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
								</form> -->
							</div>
						</div>
					</div>

					<p>
						This promo includes this year plus an additional year of Front Row Fantasy Sports.
						*Only available to the first 10,000 members!
					</p>
				</div>
			</div>
		</div>
	</div>
</div>