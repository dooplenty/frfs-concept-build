<div class="inner-page">
	<div class="hero hero-error">
		<div class="container">
			<div class="hero-heading">
				<div class="hero-logo animated">
					<img src="/assets/imgs/logo-hero.png" />
				</div>

				@if($method == "success")
					<h1 class="animated">Thank you for signing up with<br /> <strong>Front Row Fantasy Sports</strong></h1>

					<h3><a href="{{ URL::to('frontrow') }}">Continue to FrontRow</a></h3>
				@else
					<h1 class="animated">Thank you for your interest in<br /> <strong>Front Row Fantasy Sports</strong></h1>

					<h3>We hope you <a href="{{ URL::to('user/signup') }}">Join</a> us soon!</h3>
				@endif
			</div>
		</div>
	</div>
</div>