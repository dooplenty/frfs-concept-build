<div class="inner-page">
	<!-- Inner Page Login Page With Back Ground -->
	<div class="inner-login login-background">
		<div class="container">

			@if (Session::has('error'))
  				<div class="flash alert">
  					<p>{{ trans(Session::get('reason')) }}</p>
  				</div>
			@elseif (Session::has('success'))
				<div class="flash alert">
  					<p>An email with the password reset has been sent.</p>
  				</div>
			@endif

			<div class="login-container">
 				<!-- Heading -->
				<h2>Reset Password</h2>

				{{ Form::open(array('route' => 'password.request')) }}
 
				  <div class="form-group">
				  	<div class="col-sm-12">
				  		{{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
				  	</div>
				  </div>
				 
				 <div class="form-group text-center">
					<div class="col-sm-12">
			  			{{ Form::submit('Submit', array('class' => 'btn btn-danger')) }}
			  		</div>
			  	</div>
				 
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>