<div class="inner-page">
	<!-- Inner Page Login Page With Back Ground -->
	<div class="inner-login login-background">
		<div class="container">
			@if (Session::has('error'))
			  <div class="flash alert">
			  	<p>{{ trans(Session::get('reason')) }}</p>
			  </div>
			@endif
			 
			 <div class="login-container">
				{{ Form::open(array('route' => array('password.update', $token))) }}

					<div class="form-group">
						<div class="col-sm-12">
							{{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-12">
							{{ Form::text('password', null, array('class' => 'form-control', 'placeholder' => 'Password')) }}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-12">
							{{ Form::text('password_confirmation', null, array('class' => 'form-control', 'placeholder' => 'Confirm')) }}
						</div>
					</div>

					{{ Form::hidden('token', $token) }}
				 
				 	<div class="form-group text-center">
						<div class="col-sm-12">
				 			<p>{{ Form::submit('Submit', array('class' => 'btn btn-danger')) }}</p>
				 		</div>
				 	</div>
				 
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>