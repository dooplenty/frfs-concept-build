<?php

use LaravelBook\Ardent\Ardent;

class Package extends Ardent {

	protected $table = 'packages';

	protected $primaryKey = 'packageId';

	protected $guarded = array('packageId');
}