<?php

use LaravelBook\Ardent\Ardent;

class Provider extends Ardent {
	
	public function user()
	{
		return $this->belongsToMany('User', 'provider_user', 'userId', 'providerId');
	}

	protected $table = 'providers';

	protected $primaryKey = 'providerId';

	protected $guarded = array('providerId');

}