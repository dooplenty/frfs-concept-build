<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

use LaravelBook\Ardent\Ardent;

class UserLevel extends Ardent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	public $autoPurgeRedundantAttributes = true;

	public static $rules = array(
		'level' => 'required'
	);
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_levels';

	protected $guarded = array('userLevelId');

}
