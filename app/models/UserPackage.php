<?php

use LaravelBook\Ardent\Ardent;

class UserPackage extends Ardent {

	protected $table = 'user_packages';

	protected $primaryKey = 'userPackageId';
}