<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

use LaravelBook\Ardent\Ardent;

class User extends Ardent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	public $autoPurgeRedundantAttributes = true;

	public $autoHydrateEntityFromInput = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Primary Key on database table
	 *
	 * @var string
	 */
	protected $primaryKey = 'userId';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $guarded = array('userId');

	public function providers()
	{
		return $this->belongsToMany('Provider', 'provider_user', 'userId', 'providerId');
	}

	public function leagues()
	{
		return $this->hasMany('League', 'userId');
	}

	public function beforeSave()
	{
		if($this->isDirty('password')) {
			$this->password = Hash::make($this->password);
		}

		return true;
	}

	public function package()
	{
		return $this->hasManyThrough('UserPackage', 'Package', 'packageId', 'userId');
	}

}
