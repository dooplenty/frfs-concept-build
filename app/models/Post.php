<?php
use LaravelBook\Ardent\Ardent;

class Post extends Ardent {

	public function comments()
	{
		return $this->hasMany('Comment');
	}
}