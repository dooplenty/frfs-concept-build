<?php
use LaravelBook\Ardent\Ardent;

class Base extends Ardent
{
	//validation errors object
	private $errors;

	private $v;

    //return validation errors
    public function errors()
    {
    	return $this->errors;
    }

    public function validator()
    {
    	return $this->v;
    }
}