<?php

use LaravelBook\Ardent\Ardent;

class League extends Ardent {
	
	public function user()
	{
		return $this->belongsToMany('User', null, 'userId', 'leagueId');
	}

	protected $table = 'leagues';

	protected $primaryKey = 'leagueId';

	protected $guarded = array('leagueId');

	public function provider()
	{
		return $this->hasOne('Provider', 'providerId');
	}

}