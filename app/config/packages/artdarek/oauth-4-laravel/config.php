<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '',
            'client_secret' => '',
            'scope'         => array(),
        ),
		'Yahoo' => array(
		    'client_id' =>  'dj0yJmk9SlV1elhSV0l0QzFtJmQ9WVdrOVV6Rk9lazR6TjJzbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1lMA--',
		    'client_secret' => '355531a56c3a7fa26e247016f4cac3992e4d10f4',
		    'scope' => array(),
		)		

	)

);
