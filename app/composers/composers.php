<?php
View::composer(array('posts', 'posts.post'), function($view)
{
	$view->recentPosts = Post::orderBy('postId','desc')->take(5)->get();
});
