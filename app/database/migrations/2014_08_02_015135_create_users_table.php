<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('userId');
			$table->string('firstName', 32);
			$table->string('lastName', 32);
			$table->string('email', 320);
			$table->string('username', 32);
			$table->string('password', 64);
			$table->tinyInteger('termsAccepted');
			$table->string('remember_token', 100)->nullable();
			$table->tinyInteger('confirmed');
			$table->string('confirmation', 32);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
