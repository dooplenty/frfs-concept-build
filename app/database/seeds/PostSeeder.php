<?php

class PostSeeder extends Seeder {

	public function run()
	{
		$posts_array = array("
			<p>Adrian Peterson set for a big offensive season with Norv Turner. Turner was brought in this off-season to take over the offense after spending one season with the Cleveland Browns. This should be a nice transition for Turner joining a Minnesota Vikings team that has one of the NFL’s top running backs in Peterson.</p>

			<p>The move should benefit both as we have seen offenses led by Turner produce at a high level including the running back position. Over his career, Turner has coached a variety of the top running backs that include Emmitt Smith, LaDainian Tomlinson, Ricky Williams and Frank Gore.</p>

			<p>Turner has been a part of some good rushing attacks, including one of his best rushing attacks with the Dallas Cowboys between 1991 and 1993. Emmitt Smith was the main back in that system that saw them finish in the top five in two of three seasons. In 92, the Cowboys finished fifth overall with 132.6 rushing yards per game and second with 20 total rushing touchdowns. The team followed that up with the second most rushing yards per game at 135.1 and second most touchdowns with 20.</p>

			<p>In his career, Turner has had six different teams finish in the top 10 in rushing yards per game. He has also had eight different teams finish in the top 10 in total rushing touchdowns. The best season he oversaw included the 2002 Miami Dolphins team that finished with 156.4 yards rushing a game and 24 total rushing touchdowns.</p>

			<p>Turner’s rushing attack has also averaged over 100 yards per game on the ground in 17 of his 23 years in the league. His teams have also rushed for double digit touchdowns in 20 different seasons, including 12 with at least 15 and five of those with at least 20 rushing touchdowns.</p>

			<p>Now that doesn’t mean Peterson needed Turner to produce at a high level as Peterson enters 2014 with six seasons with at least 1,200 rushing yards with the lone exception coming in his injured 2011 season. Peterson has also rushed for at least 10 rushing touchdowns in each of his seven seasons in the NFL.</p>

			<p>If Turner can get the passing game with Matt Cassel or Teddy Bridgewater playing at a high level the running game should open up. Turner will also look to get talented and speedy wide receiver Cordarrelle Patterson more involved in the passing game.</p>

			<p>We should all expect a big fantasy season out of number 28 Peterson.</p>
			",

			"
			<p>LeSean McCoy sits atop the 2014 NFL first round fantasy mock draft. The Philadelphia Eagles star running back experienced one his best seasons in the NFL last season under new head coach Chip Kelly. Kelly’s fast paced high tempo offense finished second in the NFL in total yards only trailing the Denver Broncos.</p>

			<p>The Eagles rushing attack finished number one overall averaging over 160 yards per game on the ground. Much of the team’s success came from McCoy who finished the season with a career high 1,607 yards. The fifth year star also averaged over 100 yards on the ground for the first time in his young NFL career.</p>

			<p>McCoy has been a big part of the teams passing game as well pulling in at least 40 passes in each of his first five NFL seasons. Last season McCoy finished with 52 catches for 539 yards and two touchdown receptions.</p>

			<h3>12 Team First Round Mock Draft:</h3>
			<ol>
				<li>LeSean McCoy: We went back and forth with him and Jamaal Charles for the first pick. In the end we picked McCoy simply based on the Eagles schedule verses the Kanas City Chiefs schedule.</li>
				<li>Jamaal Charles: Charles led the league with 12 rushing touchdowns last season and helped pace the Chiefs offense. As we stated above the two teams each play San Francisco 49ers and Seattle Seahawks but the Chiefs also play the New York Jets as well as the other three AFC West teams that finished in the top 15 in rushing yards allowed per game.</li>
				<li>Matt Forte: We have seen Forte drop in some fantasy drafts but we don’t see how as he offers teams a dual threat option. Since entering the NFL in 2008, Forte has topped the 1,300-yard mark in combined yards in each of his six NFL seasons.</li>
				<li>Adrian Peterson: The former 2,000-yard rusher should benefit from having Norv Turner calling the offensive plays in 2014. If Peterson can get solid quarterback play he could produce another 2,000-yard season.</li>
				<li>Calvin Johnson: Johnson is simply the best receiver in the game and we expect him to continue to produce that way. The Detroit Lions also added some key weapons to help take some of the double and triple teams off of Johnson.</li>
				<li>Montee Ball: This should be the season that Ball takes a big step forward as the team’s main running back. The team lost leading rusher Knowshon Moreno in free agency opening the door for Ball. Ball also has an added benefit playing in one of the leagues top offenses.</li>
				<li>Eddie Lacy: Lacy put up a terrific rookie season rushing for over 1,100 yards in just 15 games. The second year running back will also benefit from a healthy Aaron Rodgers leading the offense.</li>
				<li>Demaryius Thomas: Peyton Manning and Thomas have built a connection over the past two seasons. In each season with Manning at quarterback Thomas has went over the 1,400-yard mark and double-digit touchdowns.</li>
				<li>Dez Bryant: The Dallas Cowboys wide receiver does exactly what you want from a fantasy wide receiver. That is to catch touchdowns something he has down over his four year NFL career pulling in 40 total.</li>
				<li>Alfred Morris: The third year running back could see a bigger opening in the running game as Robert Griffin III and the passing attack improves. In two seasons in the NFL, Morris has topped the 1,200-yard mark in each season and rushed for a combined 20 touchdowns.</li>
				<li>Jimmy Graham: We went back and forth on whether or not the tight end should go in the first round. The answer is yes as he is in his own class at the position unless Rob Gronkowski is 100 percent healthy.</li>
				<li>Le’Veon Bell: Bell totally fits the physical style running game the Pittsburgh Steelers have prided them selves on over the years. The Steelers running back finished strong in his final two games combing for over 200 yards and two rushing touchdowns.</li>
			</ol>",
			"
			<p>Jake Locker and Ken Whisenhunt set to turnaround the Tennessee Titans. Whisenhunt takes over as the head coach of the Titans as well as taking the responsibility of handling the offense. That is exactly what Whisenhunt did last season and is the main reason the Titans brought him into be the head coach.</p>

			<p>Whisenhunt has spent time as an offensive coordinator for three years with the Pittsburgh Steelers and last season with the San Diego Chargers. In between his two stops at offensive coordinator he spent five seasons as the Arizona Cardinals head coach.</p>

			<p>Before landing his job with the Cardinals, Whisenhunt worked as the offensive coordinator in Pittsburgh and with rookie quarterback Ben Roethlisberger. The Steelers offense was built around the run but the team finished ranked 11th and 9th in scoring in both his seasons with the team.</p>

			<p>Along the way Whisenhunt has worked with some talented players and helped developed them into better quarterbacks. Whisenhunt brought in Kurt Warner to help lead the passing attack in Arizona. The Cardinals finished with the 5th most passing yards in 2007 and 2nd most in 2008. The Cardinals offense ranked 7th in scoring in 2007 and third in scoring in 2008.</p>

			<p>After leaving the Cardinals, Whisenhunt was brought in to call the plays for the San Diego Chargers and to work with Philip Rivers. The improvement in not only Rivers but the entire Chargers offense helped him land with the Titans. Rivers saw an improvement in his passing yards, touchdowns, and the highest single completion percentage of his career. The other key stat Rivers improved on was a decline in interceptions.</p>

			<p>Now Whisenhunt has to work his magic on Titans quarterback Jake Locker. Locker has shown at times in his three year career that he has the ability to make big plays. The major concern has been his ability to stay on the field only playing in 23 games in three seasons due to injuries.</p>

			<p>Along with Locker, the Titans drafted quarterback Zach Mettengberger out of LSU and running back Bishop Sankey to help bolster the offense. The team also have spent the past few drafts picking up Kendall Wright and Justin Hunter. The Titans also signed Dexter McCluster this off-season to add a playmaker.</p>

			<p>Whisenhunt will develop a game plan that works the best for Locker involving the running backs into the passing game as well as designing special packages for McCluster. The team will also look to Sankey to carry the workload at the running back position.</p>

			<p>If Whisenhunt can rub off on Locker the Titans offense will see an improvement from ranking 19th in scoring, 22nd in total yards and 21st in passing yards.</p>
			",
			"<p>Drew Brees and Peyton Manning enter the 2014 fantasy season as the top two quarterbacks. The only question is which quarterback should go first and is their any other quarterbacks that are on the same level as these two.</p>

			<p>As we examine Drew Brees the first thing that jumps out at you is his four NFL seasons with at least five thousand yards passing. Brees is the only quarterback in the history of the NFL with multiple seasons with that many yards. The other crazy part is Brees has accomplished this in each of the past five NFL Seasons.</p>

			<p>The next stat the looks good is Brees ability to throw touchdowns something that he has accomplished 363 times during the regular season. In each of the past six seasons Brees has completed at least 33 touchdown passes. Brees also has the ability to complete big plays as he has been apart of passes of at least 75 yards in each of the past six seasons including three of at least 80 yards.</p>

			<p>All of these stats look good but when we flip over to Peyton Manning’s 2013 NFL season the argument can be made he should go number one. Manning broke the single season passing record for passing yards with 5,477 and passing touchdowns with 55.</p>

			<p>Manning has also passed for at least 4,000 yards in 13 of 15 NFL seasons. One of the two seasons he didn’t complete the feat was in 1998 as a rookie. The only other season he didn’t hit the 4,000 yard mark was during the 2005 NFL season. Manning can also throw touchdown passes completing at least 26 in each NFL season he has played in.</p>

			<p>Now after all these numbers we still don’t know who goes number one. If we compare the two teams offenses they stack up pretty well. The New Orleans Saints have the NFL’s top tight end in Jimmy Graham along with wide receiver Marques Colston. The Denver Broncos have one of the NFL’s top wide receivers in Demaryius Thomas as well as talented tight end Julius Thomas.</p>

			<p>Just as we start thinking we have made our mind up we remind our self what about Aaron Rodgers. Last season Rodgers missed seven games and still was able to hit the 2,500 yard mark and 17 touchdown passes. Prior to last season Rodgers had completed at least 28 touchdown passes in four straight seasons and had at least 3,900 yards in each season. Rodgers also added at least two rushing touchdowns in five of the last six seasons.</p>

			<p>In the end we would go with Drew Brees first followed by Peyton Manning then Aaron Rodgers!</p>"

		);

		$titles = array(
			"Adrian \"All Day\" Peterson Primed for Huge Fantasy Football Season",
			"First Round 2014 Fantasy Football Mock Draft",
			"The Titans 2014 Turnaround",
			"Drew Brees or Peyton Manning?<br/>Who’s Drafted First in Your Fantasy Football Draft"
		);

		$imgs = array(
			'adrianpeterson.jpg',
			'leseanmccoy.jpg',
			'jakelocker.jpg',
			'peytonmanning.jpg'
		);

		$meta = array(
			"Joe Bielawa/Flickr | Documentation licensed under <a href='https://creativecommons.org/licenses/by/2.0/' target='_blank'>CC BY 2.0</a> | <a href='https://www.flickr.com/photos/joebielawa/8330376594/in/set-72157632390937403'>See Original</a>",
			"Mr. Schultz | Documentation licensed under <a href='http://creativecommons.org/licenses/by-sa/3.0'>CC By-SA 3.0</a> via Wikimedia Commons | <a href='http://commons.wikimedia.org/wiki/File:Lesean_mccoy_2013_skins.JPG#mediaviewer/File:Lesean_mccoy_2013_skins.JPG'>See Original</a>",
			"Johnathan Shell/Flickr | Documentation licensed under <a href='https://creativecommons.org/licenses/by-sa/2.0/' target='_blank'>CC BY-SA 2.0</a> | <a href='https://www.flickr.com/photos/johnathonshell/7675796888/' target='_blank'>See Original</a>",
			"Jeffrey Beall/Flickr | Documentation licensed under <a href='http://creativecommons.org/licenses/by-sa/3.0'>CC By-SA 3.0</a> via Wikimedia Commons | <a href='http://www.flickr.com/photos/denverjeffrey/8031486384/' target='_blank'>See Original</a>"
		);

		foreach($posts_array as $i => $content){
			$post = new Post;
			$post->content = $content;
			$post->title = $titles[$i];
			$post->comment_count =  0;
			$post->imgFilename = $imgs[$i];
			$post->metaContent = $meta[$i];
			$post->save();
		}

	}
}