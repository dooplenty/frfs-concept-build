<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Home section
Route::get('/', 'HomeController@getIndex');

Route::get('/resources', 'HomeController@getResources');

Route::get('/comingsoon', 'HomeController@show2015');

Route::put('/contacts/signup', 'ContactController@signUpPost');

// User section
Route::get('/user', 'UserController@getIndex');

Route::get('/user/login', 'UserController@showLogin');

Route::post('/user/login', 'UserController@doLogin');

Route::get('/user/logout', 'UserController@doLogout');

Route::get('/user/signup', 'UserController@showSignup');

Route::post('/user/signup', 'UserController@doSignup');

// Passwords
Route::get('password/reset', array(
  'uses' => 'PasswordController@remind',
  'as' => 'password.remind'
));

Route::post('password/reset', array(
  'uses' => 'PasswordController@request',
  'as' => 'password.request'
));

Route::get('password/reset/{token}', array(
  'uses' => 'PasswordController@reset',
  'as' => 'password.reset'
));

Route::post('password/reset/{token}', array(
  'uses' => 'PasswordController@update',
  'as' => 'password.update'
));

Route::get('/user/confirm/{key}',['as' => 'user.confirm', 'uses' => 'UserController@confirmUser']);

// Frontrow section
Route::get('/frontrow', 'FrontRowController@getIndex')->before('auth')->before('paid');
Route::get('/frontrow', 'FrontRowController@getIndex')->before('paid');

//Blog routes
Route::get('/blog/fantasyfootball', 'BlogController@getIndex');

Route::get('/blog/post/{post}/show',['as' => 'post.show','uses' => 'PostController@showPost']);

Route::post('/blog/search', 'BlogController@getSearch');

//Blog Model Bindings
Route::bind('post', function($value, $route){
	return Post::where('postId', $value)->first();
});

//authentications
Route::get('/auth/{driver}/', 'AuthController@customAuth');

Route::post('/auth/search/{driver}/', 'AuthController@search');

Route::post('/auth/leagues/save/{driver}/', 'AuthController@save');

//package
Route::get('/user/package', 'PackageController@getIndex')->before('auth');

Route::post('user/purchase', 'PackageController@purchase')->before('auth');

Route::get('confirmation/{type}', 'PackageController@confirmation')->before('auth');

Route::post('ipn', array('uses' => 'IpnController@store', 'as' => 'ipn'));